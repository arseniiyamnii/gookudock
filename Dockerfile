FROM ubuntu:18.04
RUN apt update &&\
	 apt upgrade
RUN apt install -y tmux
RUN apt install -y curl

# install gcloud
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list &&\
	 apt-get install -y apt-transport-https ca-certificates gnupg &&\
	curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - &&\
	apt-get update && apt-get install -y google-cloud-sdk

# install kubectl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" &&\
	curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256" &&\
	install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
RUN apt install -y wget

#install helm
RUN wget https://get.helm.sh/helm-v3.4.1-linux-amd64.tar.gz &&\
	tar xvf helm-v3.4.1-linux-amd64.tar.gz &&\
	mv linux-amd64/helm /usr/local/bin &&\
	rm helm-v3.4.1-linux-amd64.tar.gz &&\
	rm -rf linux-amd64

# install kustomize
RUN curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash &&\
	mv kustomize /usr/bin/kustomize
RUN pwd
# auth to gcloud. can be after installation. RUN command can be with installation
COPY ./augmented-form-312415-d78014faf41f.json /gcloud-key.json
RUN gcloud auth activate-service-account third-account@augmented-form-312415.iam.gserviceaccount.com --key-file=gcloud-key.json
# https://stackoverflow.com/questions/45472882/how-to-authenticate-google-cloud-sdk-on-a-docker-ubuntu-image
ENV GOOGLE_APPLICATION_CREDENTIALS /gcloud-key.json
#RUN gcloud config set core/account 939023029234-compute@developer.gserviceaccount.com 
RUN gcloud config set project augmented-form-312415
COPY ./imgr.sh /bin/imgr
RUN apt install -y openssh-server
