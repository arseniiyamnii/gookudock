#!/bin/bash
if [ "$1" = "create" ]; then
	echo "input new instance name"
# ask name
	read instancename
# ask image(from list)
	echo "choose your image number"
	gcloud compute images list --uri > /tmp/imagesurls
	awk -F "/" '{print $NF}' /tmp/imagesurls > /tmp/tmpimagesurls2 && mv -f /tmp/tmpimagesurls2 /tmp/imagesurls2
	cat --number /tmp/imagesurls2
	read imagenumber
	imagename=$(sed -n "$imagenumber"p /tmp/imagesurls)
	gcloud compute instances create "$instancename" --image "$imagename" --zone us-central1-a
fi
if [ "$1" = "update" ]; then
	#ask name(from list)
	gcloud compute instances list > /tmp/instancelist
	#print name
	namesarr=($(awk -F " " 'FNR > 1 {print $1}' /tmp/instancelist))
	zonesarr=($(awk -F " " 'FNR > 1 {print $2}' /tmp/instancelist))
	awk -F " " 'FNR > 1 {print $1}' /tmp/instancelist > /tmp/tmpinstancelist && mv -f /tmp/tmpinstancelist /tmp/instancelist2
	cat --number /tmp/instancelist2
	echo "choose instance"
	read instancenumber
	instancename=$(sed -n "$instancenumber"p /tmp/instancelist2)
	gcloud compute instances update "$instancename"
fi
if [ "$1" = "delete" ]; then
	#ask name(from list)
	gcloud compute instances list > /tmp/instancelist
	#print name
	namesarr=($(awk -F " " 'FNR > 1 {print $1}' /tmp/instancelist))
	zonesarr=($(awk -F " " 'FNR > 1 {print $2}' /tmp/instancelist))
	awk -F " " 'FNR > 1 {print $1}' /tmp/instancelist > /tmp/tmpinstancelist && mv -f /tmp/tmpinstancelist /tmp/instancelist2
	cat --number /tmp/instancelist2
	echo "choose instance"
	read instancenumber
	instancename=$(sed -n "$instancenumber"p /tmp/instancelist2)
	gcloud compute instances delete "$instancename" --zone us-central1-a --quiet
fi
if [ "$1" = "list" ]; then
	#ask name(from list)
	gcloud compute instances list
fi
if [ "$1" = "ilist" ]; then
	#ask name(from list)
	gcloud compute images list --uri > /tmp/imagesurls
	awk -F "/" '{print $NF}' /tmp/imagesurls > /tmp/tmpimagesurls2 && mv -f /tmp/tmpimagesurls2 /tmp/imagesurls2
	cat --number /tmp/imagesurls2
fi

